//
//  DataTests.swift
//  DataTests
//
//  Created by Khoa Nguyen on 19/03/2022.
//

import XCTest
import Domain

@testable import Data

class DataTests: XCTestCase {

    var fileStoring: FileStoring!
    
    override func setUp() {
        fileStoring = MockFileStoring()
    }
    
    override func tearDown() {
        fileStoring = nil
    }

    func test_create_file_record_successfully() {
        let folderName = "notes"
        let newRecord = fileStoring.createFile(folder: folderName, with: "record-1.test")
        
        let allUrls = fileStoring.fetchAllFileInFolder(folder: folderName)
        
        XCTAssertEqual(allUrls.count, 1)
        XCTAssertNotNil(allUrls.first)
        XCTAssertEqual(allUrls.first!, newRecord)
    }
}
