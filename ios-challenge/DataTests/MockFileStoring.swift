//
//  MockFileStoring.swift
//  DataTests
//
//  Created by Khoa Nguyen on 20/03/2022.
//
import Domain

class MockFileStoring: FileStoring {
    
    init() { }
    
    private var urls = [URL]()
    
    func createFile(folder: String, with name: String) -> URL {
        let newFilePath = URL(string: "\(folder)/\(name)")!
        urls.append(newFilePath)
        return newFilePath
    }
    
    func fetchAllFileInFolder(folder: String) -> [URL] {
        return urls
    }
    
}
