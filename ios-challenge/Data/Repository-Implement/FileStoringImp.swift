//
//  FileStoring.swift
//  Data
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import Domain

extension FileManager: FileStoring {

    var documentsDir: URL {
        return urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    public func fetchAllFileInFolder(folder: String) -> [URL] {
        do {
            let folderURL = documentsDir.appendingPathComponent(folder)
            let contents = try contentsOfDirectory(at: folderURL, includingPropertiesForKeys: [.fileResourceTypeKey],
                                                   options: .skipsHiddenFiles)
            print("-------- Files In Folder \(folder)---------")
            for url in contents {
                print(url.description)
            }
            print("----------------End----------------")
            return contents
        } catch {
            print(error.localizedDescription)
            return []
        }
    }
    
    public func createFile(folder: String, with name: String) -> URL {
        let folderDir = documentsDir.appendingPathComponent(folder)
        if !fileExists(atPath: folderDir.path) {
            do {
                try FileManager.default.createDirectory(atPath: folderDir.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        return folderDir.appendingPathComponent(name)
    }

}
