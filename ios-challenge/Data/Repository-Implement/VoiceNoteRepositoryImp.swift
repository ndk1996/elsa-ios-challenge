//
//  VoiceNoteRepositoryImp.swift
//  Data
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import Foundation
import Domain

public class VoiceNoteRepositoryImp: VoiceNoteRepository {
  
    public var fileStoring: FileStoring
    
    public init(fileStoring: FileStoring) {
        self.fileStoring = fileStoring
    }
    
    public func loadAllVoiceNote(inFolder: String) -> [VoiceNoteEntity] {
        return fileStoring.fetchAllFileInFolder(folder: inFolder).map({
            let name = $0.pathComponents.last ?? ""
            return VoiceNoteEntity(name: name, path: $0.path)
        })
    }

}
