//
//  AppTests.swift
//  AppTests
//
//  Created by Khoa Nguyen on 21/03/2022.
//

import XCTest
import Domain
import App

class DomainMappingTest: XCTestCase {
    
    func test_as_voice_note_ui_model() {
        let entity = VoiceNoteEntity(name: "record", path: "/A/B/C")
        let modelUI = entity.toVoiceNoteUI()
        
        XCTAssertEqual(modelUI.name, "record")
        XCTAssertEqual(modelUI.path, "/A/B/C")
    }
    
    func test_as_voice_note_entity_model() {
        let modelUI = VoiceNoteUI(name: "record1", path: "/A/B/C")
        let entity = modelUI.toDomain()
        
        XCTAssertEqual(entity.name, "record1")
        XCTAssertEqual(entity.path, "/A/B/C")
    }
    
}

