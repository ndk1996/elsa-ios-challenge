//
//  TranscriptUsecases.swift
//  Domain
//
//  Created by Khoa Nguyen on 19/03/2022.
//

import Speech

public protocol TranscriptUsecase: AnyObject {
    func requestTranscribePermissions(completion: @escaping (AuthorizeTranscribe) -> Void)
    func transcribe(from model: VoiceNoteEntity, result: @escaping (Result<TranscriptEntity, Error>) -> Void)
}

public class TranscriptUsecaseImp: TranscriptUsecase {
    
    private var recognizer: SFSpeechRecognizer?

    public init() {
        recognizer = SFSpeechRecognizer()
    }
    
    public func requestTranscribePermissions(completion: @escaping (AuthorizeTranscribe) -> Void) {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            DispatchQueue.main.async {
                if authStatus == .authorized {
                    completion(.authorized)
                } else {
                    completion(.deny)
                    print("Transcription permission was declined.")
                }
            }
        }
    }
   
    public func transcribe(from model: VoiceNoteEntity,
                    result: @escaping (Result<TranscriptEntity, Error>) -> Void) {
        let url = URL(fileURLWithPath: model.path)
        
        let request = SFSpeechURLRecognitionRequest(url: url)
        
        recognizer?.recognitionTask(with: request) { (res, error) in
            if let error = error {
                result(.failure(error))
                return
            }
            
            switch res {
            case .some(let reconizeRes):
                if reconizeRes.isFinal {
                    result(.success(TranscriptEntity(text: reconizeRes.bestTranscription.formattedString)))
                }
            case nil:
                print("There was an error")
                result(.failure(NSError(domain: "", code: 1, userInfo: nil)))
            }
        }
    }
}
