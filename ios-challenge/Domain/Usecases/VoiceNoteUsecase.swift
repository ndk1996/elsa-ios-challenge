//
//  LoadAllVoiceNoteUsecase.swift
//  Domain
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import Foundation

public protocol VoiceNoteUsecase {
    func createVoiceNoteName() -> URL
    func loadAllVoiceNote(completion: @escaping (Result<[VoiceNoteEntity], Error>)-> Void)
}

public class VoiceNoteUsecaseImp: VoiceNoteUsecase {


    private let repo: VoiceNoteRepository
    private let voiceFolder = "voice-note"

    public init(repo: VoiceNoteRepository) {
        self.repo = repo
    }
    
    public func loadAllVoiceNote(completion: @escaping (Result<[VoiceNoteEntity], Error>) -> Void) {
        completion(.success(repo.loadAllVoiceNote(inFolder: voiceFolder)))
    }
    
    public func createVoiceNoteName() -> URL {
        let extensionFile = ".m4a"
        let timeStamp = Int(Date().timeIntervalSince1970)
        return repo.fileStoring.createFile(folder: voiceFolder, with: "Record-\(timeStamp)" + extensionFile)
    }
    
}
