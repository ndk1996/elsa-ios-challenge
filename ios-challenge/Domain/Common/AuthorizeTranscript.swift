//
//  AuthorizeTranscript.swift
//  Domain
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import Foundation

public enum AuthorizeTranscribe {
    case authorized
    case deny
}
