//
//  VoiceNoteRepository.swift
//  Domain
//
//  Created by Khoa Nguyen on 19/03/2022.
//

import Foundation

public protocol VoiceNoteRepository {
    var fileStoring: FileStoring { get set }
    func loadAllVoiceNote(inFolder: String) -> [VoiceNoteEntity]
}

