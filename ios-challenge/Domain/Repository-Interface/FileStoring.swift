//
//  FileStoring.swift
//  Domain
//
//  Created by Khoa Nguyen on 20/03/2022.
//

public protocol FileStoring {
    func createFile(folder: String, with name: String) -> URL
    func fetchAllFileInFolder(folder: String) -> [URL]
}

