//
//  RecordEntity.swift
//  Domain
//
//  Created by Khoa Nguyen on 19/03/2022.
//

public struct VoiceNoteEntity {
    
    public let name: String
    public let path: String
    
    public init( name: String, path: String) {
        self.name = name
        self.path = path
    }

}
