//
//  TranscriptEntity.swift
//  Domain
//
//  Created by Khoa Nguyen on 19/03/2022.
//

public struct TranscriptEntity {
    public let text: String
    
    public init(text: String) {
        self.text = text
    }
}
