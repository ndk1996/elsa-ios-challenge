//
//  MockVoiceNoteUsecase.swift
//  DomainTests
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import Domain

class MockVoiceNoteUsecase: VoiceNoteUsecase {
    
    private var models = [VoiceNoteEntity]()
    
    func clearData() {
        models.removeAll()
    }
    
    func createVoiceNoteName() -> URL {
        let path = "/com.voice.note"
        models.append(VoiceNoteEntity(name: "Mock", path: path))
        return URL(fileURLWithPath: path)
    }
    
    func loadAllVoiceNote(completion: @escaping (Result<[VoiceNoteEntity], Error>) -> Void) {
        completion(.success(models))
    }
    
    
}
