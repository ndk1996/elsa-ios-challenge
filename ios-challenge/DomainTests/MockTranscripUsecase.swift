//
//  MockTranscripUsecase.swift
//  DomainTests
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import Domain

class MockTranscripUsecase: TranscriptUsecase {
    
    init(data: [String: String] ) {
        self.data = data
    }
    
    var data = [String: String]()
    
    func requestTranscribePermissions(completion: @escaping (AuthorizeTranscribe) -> Void) {
        completion(.authorized)
    }
    
    func transcribe(from model: VoiceNoteEntity, result: @escaping (Result<TranscriptEntity, Error>) -> Void) {
        
        if let transcriptText = data[model.name] {
            result(.success(TranscriptEntity(text: transcriptText)))
        } else {
            result(.failure(NSError(domain: "transcript.faile", code: 2, userInfo: nil)))
        }
        
    }
    
    
}
