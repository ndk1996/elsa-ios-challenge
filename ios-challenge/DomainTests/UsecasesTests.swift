//
//  DomainTests.swift
//  DomainTests
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import XCTest
import Domain

class UsecasesTests: XCTestCase {

    var voiceNoteUsecase: VoiceNoteUsecase!
    var transcripUsecase: MockTranscripUsecase!
    
    override func setUp() {
        voiceNoteUsecase = MockVoiceNoteUsecase()
    }
    
    override func tearDown() {
        voiceNoteUsecase = nil
    }

    func test_create_voice_note_successfull() {
        let url = voiceNoteUsecase.createVoiceNoteName()
        
        let promise = expectation(description: "create voice note successfull")

        voiceNoteUsecase.loadAllVoiceNote { result in
            switch result {
            case .success(let models):
                XCTAssertEqual(models.count, 1)
                XCTAssertEqual(models.first!.path, url.path)
                promise.fulfill()
            case .failure(_):
                XCTExpectFailure()
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_transcribe_model_successfully() {
        let voiceNote = VoiceNoteEntity(name: "record-1", path: "")
        let data: [String: String] = [voiceNote.name: "Hello, Elsa"]
        transcripUsecase = MockTranscripUsecase(data: data)
        
        let promise = expectation(description: "transcribe successfull")
        
        transcripUsecase.transcribe(from: voiceNote) { result in
            switch result {
            case .success(let entity):
                XCTAssertEqual(entity.text, "Hello, Elsa")
                promise.fulfill()
            case .failure(_):
                XCTExpectFailure()
            }
        }
        waitForExpectations(timeout: 5, handler: nil)

    }
    
    func test_transcribe_model_fail() {
        let voiceNote = VoiceNoteEntity(name: "record-1", path: "")
        let data: [String: String] = [voiceNote.name: "Hello, Elsa"]
        transcripUsecase = MockTranscripUsecase(data: data)
        
        let promise = expectation(description: "transcribe successfull")
        
        let voiceNote2 = VoiceNoteEntity(name: "record-2", path: "")

        transcripUsecase.transcribe(from: voiceNote2) { result in
            switch result {
            case .success(_):
                break
            case .failure(let err):
                XCTAssertNotNil(err)
                promise.fulfill()
            }
        }
        waitForExpectations(timeout: 5, handler: nil)

    }
}

