//
//  AppDelegate.swift
//  ios-challenge
//
//  Created by Khoa Nguyen on 19/03/2022.
//

import UIKit
import Domain
import Data

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        initRootView()

        return true
    }

    private func initRootView() {
        let mainVC = MainViewController(voiceNoteUsecase: VoiceNoteUsecaseImp(repo: VoiceNoteRepositoryImp(fileStoring: FileManager.default)), transcriptUsecase: TranscriptUsecaseImp())
        let nav = UINavigationController(rootViewController: mainVC)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }


}

