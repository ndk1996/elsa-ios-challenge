//
//  CreateVoiceNoteViewController.swift
//  App
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import UIKit
import Domain
import AVFoundation

protocol CreateVoiceNoteViewControllerDelegate: AnyObject {
    func didFinishRecordVoiceNote()
}

final class CreateVoiceNoteViewController: BaseViewController {
    @IBOutlet private weak var btnRecord: UIButton!
    
    private let recordingSession: AVAudioSession = AVAudioSession.sharedInstance()
    private var audioRecorder: AVAudioRecorder?
    
    private let voiceNoteUsecase: VoiceNoteUsecase
    weak var delegate: CreateVoiceNoteViewControllerDelegate?
    
    init(voiceNoteUsecase: VoiceNoteUsecase) {
        self.voiceNoteUsecase = voiceNoteUsecase
        super.init(nibName: "\(Self.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestMic()
        
    }
    
    @IBAction func handleCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    private func requestMic() {
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    self.btnRecord.isEnabled = allowed
                }
            }
        } catch {
            btnRecord.isEnabled = false
        }
    }
    
    private func startRecording() {
        let audioFilename = voiceNoteUsecase.createVoiceNoteName()
        print("audioFilename url: ", audioFilename.path)

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
            AVMetadataKey.commonKeyLocation.rawValue: "HCM"
        ] as [String : Any]

        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()
            btnRecord.setTitle("Tap to Stop", for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    private func finishRecording(success: Bool) {
        audioRecorder?.stop()
        audioRecorder = nil
        btnRecord.setTitle("Tap to start record", for: .normal)
        
        if success {
            delegate?.didFinishRecordVoiceNote()
            showAlert(title: "Successfully",
                      message: "Your voice note is saved.") { [weak self] in
                self?.dismiss(animated: true, completion: nil)
            }
        } else {
            showAlert(title: "Error",
                      message: "Can not save your record. Please try again!")
        }
    }

    @IBAction func handleRecord(_ sender: Any) {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
}
extension CreateVoiceNoteViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}
