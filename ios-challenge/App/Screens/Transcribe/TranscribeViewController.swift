//
//  TranscribeViewController.swift
//  App
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import UIKit
import Domain

final class TranscribeViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTranscript: UILabel!
    
    private let voiceNote: VoiceNoteUI
    private let transcriptUsecase: TranscriptUsecase

    init(voiceNote: VoiceNoteUI, transcriptUsecase: TranscriptUsecase) {
        self.voiceNote = voiceNote
        self.transcriptUsecase = transcriptUsecase
        super.init(nibName: "\(Self.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = voiceNote.name
        
        transcriptUsecase.requestTranscribePermissions { [weak self] authorize in
            guard let self = self else { return }

            if authorize == .authorized {
                self.transcripting(with: self.voiceNote)
            }
        }
        
    }

   private func transcripting(with model: VoiceNoteUI) {
       transcriptUsecase.transcribe(from: model.toDomain(), result: { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let transcript):
                self.lblTranscript.text = transcript.text
            case .failure(let err):
                self.showAlert(title: "Error", message: err.localizedDescription)
            }
        })
    }
}
