//
//  VoiceNoteCell.swift
//  App
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import UIKit
import Domain
import AVFoundation

protocol VoiceNoteCellDelegate: AnyObject {
    func didTapPlayPause(_ cell: VoiceNoteCell, state: PlayState, at indexPath: IndexPath)
}

class VoiceNoteCell: UITableViewCell {

    @IBOutlet private weak var btnPlayStop: UIButton!
    @IBOutlet private weak var lblName: UILabel!
    weak var delegate: VoiceNoteCellDelegate?
    private var indexPath: IndexPath?
    private var player: AVAudioPlayer?
    private var voiceNote: VoiceNoteUI?

    var playState: PlayState = .stop {
        didSet {
            switch playState {
            case .stop:
                btnPlayStop.setImage(.init(systemName: "play"), for: .normal)
            case .playing:
                btnPlayStop.setImage(.init(systemName: "stop"), for: .normal)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        player = nil
        voiceNote = nil
    }
    
    func setData(_ voiceNote: VoiceNoteUI, indexPath: IndexPath) {
        self.indexPath = indexPath
        self.voiceNote = voiceNote
        lblName.text = voiceNote.name
        player = try? AVAudioPlayer(contentsOf: URL(fileURLWithPath: voiceNote.path))
    }
    
    @IBAction func handlePlayPause(_ sender: Any) {
        guard let indexPath = indexPath else { return }
        switch playState {
        case .stop:
            delegate?.didTapPlayPause(self, state: .playing, at: indexPath)
        case .playing:
            delegate?.didTapPlayPause(self, state: .stop, at: indexPath)
        }
    }
    
    func playVoiceNote() {
        playState = .playing
        self.player?.delegate = self
        self.player?.volume = 1.0
        self.player?.prepareToPlay()
        self.player?.play()
    }
    
    func stopVoiceNote() {
        playState = .stop
        player?.stop()
    }
}
extension VoiceNoteCell: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playState = .stop
    }
}
