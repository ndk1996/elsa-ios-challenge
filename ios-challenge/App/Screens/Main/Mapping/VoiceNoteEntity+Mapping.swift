//
//  VoiceNoteEntity+Mapping.swift
//  App
//
//  Created by Khoa Nguyen on 21/03/2022.
//

import Domain

extension VoiceNoteUI {
    public func toDomain() -> VoiceNoteEntity {
        VoiceNoteEntity(name: name, path: path)
    }
}
extension VoiceNoteEntity {
    public func toVoiceNoteUI() -> VoiceNoteUI {
        VoiceNoteUI(name: name, path: path)
    }
}
