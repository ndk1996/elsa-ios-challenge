//
//  MainViewController.swift
//  app
//
//  Created by Khoa Nguyen on 19/03/2022.
//

import AVFoundation
import UIKit
import Domain

final class MainViewController: BaseViewController  {
    @IBOutlet weak var tableView: UITableView!
    
    private let voiceNoteUsecase: VoiceNoteUsecase
    private let transcriptUsecase: TranscriptUsecase

    init(voiceNoteUsecase: VoiceNoteUsecase,
         transcriptUsecase: TranscriptUsecase ) {
        self.voiceNoteUsecase = voiceNoteUsecase
        self.transcriptUsecase = transcriptUsecase
        super.init(nibName: "\(Self.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var voiceNotes = [VoiceNoteUI]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Voice Note"
        initSetup()
        loadAllVoiceNote()
    }
    
    private func loadAllVoiceNote() {
        voiceNoteUsecase.loadAllVoiceNote { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let entities):
                self.voiceNotes = entities.map({ $0.toVoiceNoteUI() })
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let err):
                self.showAlert(title: "Error", message: err.localizedDescription)
            }
        }
    }
    
    @objc
    private func addVoiceNote() {
        let vc = CreateVoiceNoteViewController(voiceNoteUsecase: self.voiceNoteUsecase)
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func initSetup() {
        tableView.register(nib: VoiceNoteCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addVoiceNote))
    }
   
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        voiceNotes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(nib: VoiceNoteCell.self, at: indexPath) else {
            fatalError("Can not find cell VoiceNoteCell")
        }
        let voice = voiceNotes[indexPath.row]
        cell.delegate = self
        cell.setData(voice, indexPath: indexPath)
        cell.selectionStyle = .none
        
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let voiceNote = voiceNotes[indexPath.row]
        let vc = TranscribeViewController(voiceNote: voiceNote, transcriptUsecase: TranscriptUsecaseImp())
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension MainViewController: VoiceNoteCellDelegate {
    func didTapPlayPause(_ cell: VoiceNoteCell, state: PlayState, at indexPath: IndexPath) {
        switch state {
        case .playing:
            cell.playVoiceNote()
        case .stop:
            cell.stopVoiceNote()
        }
    }
}
extension MainViewController: CreateVoiceNoteViewControllerDelegate {
    func didFinishRecordVoiceNote() {
        loadAllVoiceNote()
    }
}
