//
//  VoiceNoteUI.swift
//  App
//
//  Created by Khoa Nguyen on 21/03/2022.
//

import Foundation
import Domain

public struct VoiceNoteUI {
    
    public let name: String
    public let path: String
    
    public init(name: String, path: String) {
        self.name = name
        self.path = path
    }

}

