//
//  BaseViewController.swift
//  App
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import UIKit

class BaseViewController: UIViewController {
    lazy var screenName: String = {"\(Self.self)"}()
    
    deinit {
        debugPrint("🚀 \(screenName) deinit")
    }
}
