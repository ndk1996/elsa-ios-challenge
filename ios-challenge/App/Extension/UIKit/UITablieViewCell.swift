//
//  UITablieViewCell.swift
//  App
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import UIKit

extension UITableViewCell {
    static var reuseIdentifier: String {
        String(describing: Self.self)
    }
}
extension UITableView {
    func register<T: UITableViewCell>(nib: T.Type, bundle: Bundle? = nil) {
        let className = nib.reuseIdentifier
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forCellReuseIdentifier: className)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(nib: T.Type, at indexPath: IndexPath) -> T? {
        dequeueReusableCell(withIdentifier: nib.reuseIdentifier, for: indexPath) as? T
    }
}
