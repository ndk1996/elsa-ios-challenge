//
//  UIViewController.swift
//  App
//
//  Created by Khoa Nguyen on 20/03/2022.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okHandle = UIAlertAction(title: "OK", style: .default) { _ in
            completion?()
        }
        alertController.addAction(okHandle)
        present(alertController, animated: true, completion: nil)
    }
}
