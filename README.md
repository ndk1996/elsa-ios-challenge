# ELSA Challenge for iOS Developers


## Problem

Create a Voice Note application that records audio and transcribes it to text. Location information shall be captured for each note.

## Basic Functionality

A user should be able to create a note by starting to record audio
Notes should be created with the audio recording and transcribed using some ASR/Voice-to-Text service (e.g. Google ASR API or others of your choice)
Users should be able to see a list of their notes
Users should be able to select a note and view the transcribed text, playback the voice recording, and view any metadata that the note might have.
Unit Test for these features is a MUST

a/ Project is using:

- Pure Swift
- Framework: UIKit, XCTest, Speech (recognize voice)
- Dependency Manager: Swift Package Manager
- Clean architrecture [here](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html), use this as high level guideline.
- Being devided in 3 layers:
  - Domain Layer = Entities + Use Cases + Repositories Interfaces
  - Data Layer = Repositories Implementations
  - Presentation Layer (MVC) = Model + View + Controller 
- SOLID principles [here](https://www.google.com/search?sxsrf=ALeKk01XtOekOpJvhSePYEwrjdVNe2ZNfw%3A1593035218071&ei=0snzXo7VA8i2kwXo66iQAw&q=solid+principles+origin&oq=solid+principles+origi&gs_lcp=CgZwc3ktYWIQAxgAMgIIADoECAAQRzoECAAQQzoGCAAQFhAeOgcIABAUEIcCOggIABAWEAoQHlC6UljoYWCuaGgCcAF4AIABYogBtASSAQE4mAEAoAEBqgEHZ3dzLXdpeg&sclient=psy-ab)

b/ I have structured the project into 3 main layers:

```
    - Domain: this contains 2 main things:
        - Entities: represent for Enterprise business roles
        - Usecases: represent for Application roles
    - Presentation: Model + View + Controller by using MVC
    - Data: Provide some services to store data.
```

Beside that, there are `Extension, Resources` folders, ... do what their names say.

Some key frameworks:

    - XCTest: use for writing unit tests.

c/ Run project by open `ios_challenge.xcodeproj` file with scheme 'App'
*- Note: There are some errors when you are requesting to transcribe voice with framework Speech on Simulator (workaround: Erase all contents and settings).
